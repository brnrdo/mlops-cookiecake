import pandera as pa

from pandera import Column, DataFrameSchema, Check, Index

from pandas import DataFrame

schema = DataFrameSchema(
    {
        "column1": Column(int),
        "column2": Column(float, Check(lambda s: s < -1.2)),
        # you can provide a list of validators
        "column3": Column(
            str,
            [
                Check(lambda s: s.str.startswith("value")),
                Check(lambda s: s.str.split("_", expand=True).shape[1] == 2),
            ],
        ),
    },
    index=Index(int),
    strict=True,
    coerce=True,
)


@pytest.fixture()
def train_df() -> DataFrame:
    raise NotImplementedError()


def test_TrainDF_Schema(
    train_dataset: DataFrame,
):
    assert schema.validate(train_dataset)
