import mlflow
import catboost
import dvc.api

from sklearn.datasets import load_breast_cancer

params = dvc.api.params_show()

learning_rate = params["learning_rate"]
iterations = params["iterations"]
depth = params["depth"]
loss_function = params["loss_function"]


if __name__ == "__main__":
    bc = load_breast_cancer(as_frame=True)
    x = bc.data
    y = bc.target

    cb_model = catboost.CatBoostClassifier(
        iterations=iterations,
        learning_rate=learning_rate,
        depth=depth,
        loss_function=loss_function,
    ).fit(x, y)
    mlflow.catboost.save_model(cb_model, "./mlflow_model")
